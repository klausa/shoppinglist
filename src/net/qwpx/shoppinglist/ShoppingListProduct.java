package net.qwpx.shoppinglist;

import android.os.Parcel;
import android.os.Parcelable;

public class ShoppingListProduct implements Parcelable {
    public static final Parcelable.Creator<ShoppingListProduct> CREATOR = new Parcelable.Creator<ShoppingListProduct>() {
        @Override
        public ShoppingListProduct createFromParcel(Parcel in) {
            return new ShoppingListProduct(in);
        }

        @Override
        public ShoppingListProduct[] newArray(int size) {
            return new ShoppingListProduct[size];
        }
    };

    private String name;
    private Integer count = 1;
    private Boolean is_bought = false;

    public ShoppingListProduct(String name) {
        setName(name);
    }

    public ShoppingListProduct(String name, String count) {
        setName(name);
        if (!count.isEmpty()) {
            setCount(Integer.parseInt(count));
        }
    }

    public ShoppingListProduct(Parcel in) {
        setName(in.readString());
        setCount(in.readInt());
        setIs_bought((in.readByte() == 1));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.count);
        dest.writeByte((byte) (this.is_bought ? 1 : 0));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count.toString();
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Boolean getIs_bought() {
        return is_bought;
    }

    public void setIs_bought(Boolean is_bought) {
        this.is_bought = is_bought;
    }

}