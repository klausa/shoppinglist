package net.qwpx.shoppinglist;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

public class ShoppingListActivity extends ListActivity {

    private Button addButton;
    private ArrayList<ShoppingListProduct> itemsInList;
    private ShoppingListAdapter adapter;
    static private int ADD_NEW_ITEM_INTENT = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        if (savedInstanceState != null) {
            itemsInList = savedInstanceState
                    .getParcelableArrayList("listValue");
        } else {
            itemsInList = new ArrayList<ShoppingListProduct>();
        }

        adapter = new ShoppingListAdapter(this, itemsInList);
        setListAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ADD_NEW_ITEM_INTENT) {
            if (resultCode == RESULT_OK) {
                ShoppingListProduct newProduct = new ShoppingListProduct(
                        data.getStringExtra("value"),
                        data.getStringExtra("count"));
                adapter.add(newProduct);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("listValue", itemsInList);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shopping_list, menu);
        menu.findItem(R.id.menu_item_new).setShowAsAction(
                MenuItem.SHOW_AS_ACTION_ALWAYS);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_item_new:
            Intent newIntent = new Intent(ShoppingListActivity.this,
                    AddProductActivity.class);
            startActivityForResult(newIntent, ADD_NEW_ITEM_INTENT);
            break;

        }
        return super.onOptionsItemSelected(item);
    }

}
