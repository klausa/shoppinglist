package net.qwpx.shoppinglist;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class AddProductActivity extends Activity {

    private EditText itemInput;
    private EditText countInput;
    private Button addButton;

    // private ArrayList<String> itemsInList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        addButton = (Button) findViewById(R.id.add_new_item_buttom);
        itemInput = (EditText) findViewById(R.id.newItemInput);
        countInput = (EditText) findViewById(R.id.newItemCount);

        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);

        itemInput.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                    int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                    int count) {

                if (s.toString().isEmpty()) {
                    addButton.setEnabled(false);
                } else {
                    addButton.setEnabled(true);
                }

            }
        });

        // itemInput.
        addButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent resultIntent = new Intent();
                resultIntent.putExtra("value", itemInput.getText().toString());
                resultIntent.putExtra("count", countInput.getText().toString());
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shopping_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            break;

        }
        return super.onOptionsItemSelected(item);
    }

}
