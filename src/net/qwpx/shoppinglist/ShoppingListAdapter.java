package net.qwpx.shoppinglist;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class ShoppingListAdapter extends ArrayAdapter<ShoppingListProduct> {

    LayoutInflater inflater;
    ArrayList<ShoppingListProduct> productsList;

    public ShoppingListAdapter(Context context,
            ArrayList<ShoppingListProduct> list) {

        super(context, R.layout.shopping_list_row_item,
                R.id.row_item_text_view, list);
        inflater = LayoutInflater.from(context);
        productsList = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        final ShoppingListProduct product = getItem(position);

        if (rowView == null) {
            rowView = inflater.inflate(R.layout.shopping_list_row_item, parent,
                    false);
        }

        TextView textView = (TextView) rowView
                .findViewById(R.id.row_item_text_view);
        textView.setText(product.getName());

        TextView countView = (TextView) rowView
                .findViewById(R.id.row_item_count_view);
        countView.setText(product.getCount());

        CheckBox check = (CheckBox) rowView.findViewById(R.id.checkBox1);
        check.setOnCheckedChangeListener(null);
        // apparently listeners are kept too when we're reusing
        // views... don't ask me, i'm not the one to come up with such retared
        // idea.
        check.setChecked(product.getIs_bought());

        check.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                    boolean isChecked) {

                product.setIs_bought(isChecked);

            }

        });

        return rowView;

    }

    @Override
    public void add(ShoppingListProduct item) {
        productsList.add(item);
        notifyDataSetChanged();
    }

}
